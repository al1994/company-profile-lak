module.exports = {
  HOST: "localhost",
  USER: "root",
  PASSWORD: "",
  DB: "lembagaakuntansi_db",
  dialect: "mysql",
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000,
  },
};

// module.exports = {
//   HOST: "localhost",
//   USER: "lemc8815_lak_db",
//   PASSWORD: "3@x6G{2dK-fO",
//   DB: "lemc8815_lak_db",
//   dialect: "mysql",
//   pool: {
//     max: 5,
//     min: 0,
//     acquire: 30000,
//     idle: 10000,
//   },
// };
