exports.monthNamesId = [
  "Januari",
  "Februari",
  "Maret",
  "April",
  "Mei",
  "Juni",
  "Juli",
  "Agustus",
  "September",
  "Oktober",
  "November",
  "Desember",
];

exports.dayNames = [
  "Senin",
  "Selasa",
  "Rabu",
  "Kamis",
  "Jum'at",
  "Sabtu",
  "Minggu",
];

exports.whiteListExtImage = [
  "image/jpg",
  "image/JPG",
  "image/png",
  "image/PNG",
  "image/jpeg",
  "image/JPEG",
  "image/svg",
  "image/SVG",
  "image/gif",
  "image/GIF",
];
