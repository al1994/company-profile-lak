// const { diskStorage } = require("multer");
// const path = require("path");

const { monthNamesId, whiteListExtImage } = require("./const.js");

exports.setCodeCountryAndSpace = (numberPhone) => {
  const newNumber = numberPhone.replace(/(\d{4})(\d{4})(\d{4})/, "$1 $2 $3");
  let firstNumber = newNumber.toString().charAt(0);
  if (firstNumber == "0") {
    var replaced = newNumber.replace(firstNumber, "+62 ");
    return replaced;
  }
  return newNumber;
};

exports.setCodeCountry = (numberPhone) => {
  let firstNumber = numberPhone.toString().charAt(0);
  if (firstNumber == "0") {
    var replaced = numberPhone.replace(firstNumber, "62");
    return replaced.trim();
  }
  return numberPhone.trim();
};

exports.emailAddBr = (email) => {
  const array = email.split("@");
  return `${array[0]}<br/>@${array[1]}`;
};

exports.DateFormat = (date) => {
  return `${date.getDate()} ${
    monthNamesId[date.getMonth()]
  } ${date.getFullYear()}`;
};

exports.convertDate = (date) => {
  const newDate = new Date(date);
  var dd = String(newDate.getDate()).padStart(2, "0");
  var mm = String(newDate.getMonth() + 1).padStart(2, "0"); //January is 0!
  var yyyy = newDate.getFullYear();

  return `${yyyy}-${mm}-${dd}`;
};

// exports.SentenceCase = (string) => {
//   var sentence = string.trim().toLowerCase().split(" ");
//   for (var i = 0; i < sentence.length; i++) {
//     sentence[i] = sentence[i][0].toUpperCase() + sentence[i].slice(1);
//   }
//   return sentence.join(" ");
// };

exports.SentenceCase = (str) => {
  return str.replace(/\w\S*/g, function (txt) {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  });
};

exports.ImgExtIsValid = (mimetype) => {
  if (whiteListExtImage.includes(mimetype)) {
    let ext = mimetype.split("/");
    return ext[1];
  }
  return false;
};

exports.imageFilter = function (req, file, cb) {
  // Accept images only
  if (!file.originalname.match(/\.(jpg|JPG|jpeg|JPEG|png|PNG|gif|GIF)$/)) {
    req.fileValidationError = "Only image files are allowed!";
    return cb(new Error("Only image files are allowed!"), false);
  }
  cb(null, true);
};
