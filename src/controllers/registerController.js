const { users } = require("../models");

exports.index = (req, res) => {
  res.render("pages/register", {
    url: "http://localhost:5050/",
    // Kirim juga library flash yang telah di set
    colorFlash: req.flash("color"),
    statusFlash: req.flash("status"),
    pesanFlash: req.flash("message"),
  });
};

exports.store = async (req, res) => {
  try {
    let username = req.body.username;
    let password = req.body.password;

    if (username && password) {
      users
        .create({
          username: username,
          password: password,
        })
        .then(function (users) {
          if (users) {
            res.redirect("/login");
            return;
          } else {
            req.flash("color", "danger");
            req.flash("status", "Oops..");
            req.flash("message", "Data tidak tersimpan");
            res.redirect("/register");
          }
        });

      // if (getUser.length > 0) {
      //     Jika data ditemukan, set sesi user tersebut menjadi true
      //     req.session.loggedin = true;
      //     req.session.userid = results[0].user_id;
      //     req.session.username = results[0].user_name;
      //     res.redirect('/admin');
      // } else {
      //     Jika data tidak ditemukan, set library flash dengan pesan error yang diinginkan
      //     req.flash('color', 'danger');
      //     req.flash('status', 'Oops..');
      //     req.flash('message', 'Akun tidak ditemukan');
      //     res.redirect('/login');
      // }
    } else {
      res.redirect("/register");
      res.end();
    }
  } catch (error) {
    res.redirect("/register");
    res.end();
  }
};

// const authenticateUserWithemail = (user) => {
//   return new Promise((resolve, reject) => {
//     try {
//         users
//         .findOne({
//           where: {
//             user_email: user.userName, // user email
//           },
//         })
//         .then(async (response) => {
//           if (!response) {
//             resolve(false);
//           } else {
//             if (
//               !response.dataValues.password ||
//               !(await response.validPassword(
//                 user.password,
//                 response.dataValues.password
//               ))
//             ) {
//               resolve(false);
//             } else {
//               resolve(response.dataValues);
//             }
//           }
//         });
//     } catch (error) {
//       const response = {
//         status: 500,
//         data: {},
//         error: {
//           message: "user match failed",
//         },
//       };
//       reject(response);
//     }
//   });
// };
