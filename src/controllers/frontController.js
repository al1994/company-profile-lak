const { Op } = require("sequelize");
const {
  profile,
  news,
  bimtek_sub,
  bimtek,
  schedule,
  pack,
  gallery,
  galleryFile,
} = require("../models");

const {
  DateFormat,
  SentenceCase,
  setCodeCountryAndSpace,
  emailAddBr,
} = require("../config/helperFunction.js");

function checkProfile() {
  return profile.findOne();
}

exports.getProfile = async (_, res) => {
  try {
    const result = await Promise.all([
      checkProfile(),
      bimtek.findAll({
        where: {
          id: { [Op.ne]: 0 },
          isShow: "1",
          deleted: "0",
        },
        limit: 10,
      }),
      bimtek.findAll({
        include: [
          {
            model: schedule,
            required: false,
            where: {
              status: "open",
              deleted: "0",
            },
          },
        ],
        where: {
          isShow: "1",
          deleted: "0",
        },
      }),
      galleryFile.findAll({
        where: {
          isShow: "1",
          deleted: "0",
        },
        order: [["createdAT", "DESC"]],
        limit: 8,
      }),
      news.findAll({
        where: {
          isShow: "1",
          deleted: "0",
        },
        order: [["createdAT", "DESC"]],
        limit: 6,
      }),
    ]);

    res.render("front/pages/index", {
      profile: result[0],
      bimtek: result[1],
      schedule: result[2],
      galery: result[3],
      news: result[4],
      date: DateFormat,
      code: setCodeCountryAndSpace,
      emailAddBr: emailAddBr,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.getPublication = async (_, res) => {
  try {
    const result = await Promise.all([
      checkProfile(),
      news.findAll({
        where: {
          isShow: "1",
          deleted: "0",
        },
        order: [["createdAT", "DESC"]],
      }),
    ]);

    res.render("front/pages/publication", {
      profile: result[0],
      news: result[1],
      date: DateFormat,
      code: setCodeCountryAndSpace,
      emailAddBr: emailAddBr,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.showPublication = async (req, res) => {
  try {
    const result = await Promise.all([
      checkProfile(),
      news.findOne({
        where: {
          id: req.params.id,
          isShow: "1",
          deleted: "0",
        },
      }),
    ]);

    res.render("front/pages/publicationShow", {
      profile: result[0],
      news: result[1],
      date: DateFormat,
      code: setCodeCountryAndSpace,
      emailAddBr: emailAddBr,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.getBimtek = async (_, res) => {
  try {
    const result = await Promise.all([
      checkProfile(),
      bimtek_sub.findAll({
        where: {
          isShow: "1",
          deleted: "0",
        },
      }),
    ]);

    res.render("front/pages/bimtek", {
      profile: result[0],
      bimtek_sub: result[1],
      code: setCodeCountryAndSpace,
      emailAddBr: emailAddBr,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.showBimtek = async (req, res) => {
  try {
    const result = await Promise.all([
      checkProfile(),
      bimtek_sub.findOne({
        where: {
          id: req.params.id,
          isShow: "1",
          deleted: "0",
        },
        include: [
          {
            model: bimtek,
            where: {
              isShow: "1",
              deleted: "0",
            },
          },
        ],
      }),
    ]);

    res.render("front/pages/bimtekShow", {
      profile: result[0],
      bimtek_sub: result[1],
      code: setCodeCountryAndSpace,
      emailAddBr: emailAddBr,
      sentenceCase: SentenceCase,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.getSchedule = async (_, res) => {
  try {
    const result = await Promise.all([
      checkProfile(),
      schedule.findAll({
        include: [
          {
            model: bimtek,
            // required: false,
            // where: {
            //   status: "open",
            // },
            // order: [["dateStart", "DESC"]],
          },
        ],
        where: {
          isShow: "1",
          deleted: "0",
          status: "open",
        },
        order: [["dateStart", "ASC"]],
      }),
    ]);

    res.render("front/pages/schedule", {
      profile: result[0],
      schedule: result[1],
      dateFormat: DateFormat,
      sentenceCase: SentenceCase,
      code: setCodeCountryAndSpace,
      emailAddBr: emailAddBr,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.showSchedule = async (req, res) => {
  try {
    const result = await Promise.all([
      checkProfile(),
      schedule.findOne({
        where: {
          id: req.params.id,
          isShow: "1",
          deleted: "0",
        },
        include: [
          {
            model: pack,
          },
          {
            model: bimtek,
          },
        ],
      }),
      pack.findAll(),
    ]);

    res.render("front/pages/scheduleShow", {
      profile: result[0],
      schedule: result[1],
      pack: result[2],
      dateFormat: DateFormat,
      date: DateFormat,
      code: setCodeCountryAndSpace,
      emailAddBr: emailAddBr,
      sentenceCase: SentenceCase,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.getAbout = async (_, res) => {
  try {
    const getProfile = await checkProfile();

    res.render("front/pages/about", {
      profile: getProfile,
      code: setCodeCountryAndSpace,
      emailAddBr: emailAddBr,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.getGalery = async (_, res) => {
  try {
    const result = await Promise.all([
      checkProfile(),
      gallery.findAll({
        include: [
          {
            model: galleryFile,
            required: false,
            // where: {
            //   status: "waiting",
            // },
          },
        ],
        where: {
          isShow: "1",
          deleted: "0",
        },
        order: [["createdAT", "DESC"]],
      }),
    ]);

    res.render("front/pages/galery", {
      profile: result[0],
      gallery: result[1],
      code: setCodeCountryAndSpace,
      emailAddBr: emailAddBr,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};
