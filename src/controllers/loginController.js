// const { User, validPassword } = require("../models/userModel.js");
const { users } = require("../models");

exports.login = async (req, res) => {
  res.render("pages/login", {
    url: "http://localhost:5050/",
    // Kirim juga library flash yang telah di set
    colorFlash: req.flash("color"),
    statusFlash: req.flash("status"),
    pesanFlash: req.flash("message"),
  });
};

exports.auth = async (req, res) => {
  try {
    const username = req.body.username;
    const password = req.body.password;

    if (username && password) {
      const getUser = await users.findOne({
        where: {
          username: username,
        },
      });

      if (getUser) {
        const isValidated = await users.validPassword(
          password,
          getUser.password
        );

        if (isValidated) {
          req.session.loggedin = true;
          req.session.userid = getUser.id;
          req.session.username = getUser.username;
          res.redirect("/admin/schedule");
          return false;
        }
      }
    }

    req.flash("color", "danger");
    req.flash("status", "Oops..");
    req.flash("message", "Akun tidak ditemukan");
    res.redirect("/login");
    res.end();
  } catch (error) {
    console.log(error);
    res.redirect("/login");
    res.end();
  }
};

exports.logout = async (req, res) => {
  req.session.destroy((err) => {
    if (err) {
      return console.log(err);
    }
    // Hapus cokie yang masih tertinggal
    res.clearCookie("secretname");
    res.redirect("/login");
  });
};
