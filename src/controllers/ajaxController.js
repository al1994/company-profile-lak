const { bimtek, schedule } = require("../models");

exports.getScheduleAjax = async (_, res) => {
  try {
    const result = await schedule.findAll({
      include: [
        {
          model: bimtek,
          // required: false,
          //   where: {
          //     isShow: "aa",
          //   },
        },
      ],
      where: {
        isShow: "1",
        deleted: "0",
      },
      order: [["createdAT", "DESC"]],
    });

    if (result.length) {
      res.send({
        data: result,
        status: "success",
        message: `${result.length} jadwal ditemukan`,
      });
    } else {
      res.status(404).send({
        status: "not found",
        message: `Jadwal tidak ditemukan`,
      });
    }
  } catch (error) {
    res.status(500).send({
      status: "failed",
      message: "Data berhasil dihapus!",
    });
  }
};

exports.closeSchedule = async (req, res) => {
  const id = req.params.id;

  schedule
    .update(req.body, {
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          status: "success",
          message: "Jadwal berhasil diperbarui!",
        });
      } else {
        throw {
          status: "failed",
          message: "Jadwal tidak ditemukan",
          code: 404,
        };
      }
    })
    .catch((err) => {
      const errorCode = err.status ? err.code : 500;
      res.status(errorCode).send({
        name: err.name,
        message: err.status
          ? err.message
          : "Terjadi kesalahan, silahkan kembali beberapa saat lagi",
      });
    });
};
