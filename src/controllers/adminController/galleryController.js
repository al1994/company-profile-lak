const { gallery, galleryFile } = require("../../models");
const { DateFormat } = require("../../config/helperFunction.js");

exports.getGallery = async (req, res) => {
  try {
    const getGallery = await gallery.findAll({
      include: [
        {
          model: galleryFile,
          required: false,
          // where: {
          //   status: "waiting",
          // },
        },
      ],
      where: {
        isShow: "1",
        deleted: "0",
      },
      order: [["createdAT", "DESC"]],
    });

    if (getGallery.length > 0) {
      res.render("admin/pages/gallery", {
        getGallery: getGallery,
        color: req.flash("color"),
        message: req.flash("message"),
        dateFormat: DateFormat,
      });
    } else {
      throw new Error("Tidak Terdapat Galeri");
    }
  } catch (error) {
    res.render("admin/pages/gallery", {
      color: "danger",
      message: error.message,
      getGallery: [],
    });
  }
};

exports.storeGallery = async (req, res) => {
  if (req.body.images.length <= 0) {
    req.flash("color", "danger");
    req.flash("message", "Tidak terdapat unggahan file");
    res.redirect("/admin/gallery");
  } else {
    gallery
      .create({
        title: req.body.title,
      })
      .then(function (x) {
        var arr = req.body.images.map((image) => ({
          galeryId: x.id,
          image: image,
        }));

        galleryFile.bulkCreate(arr).then(function (x) {
          req.flash("color", "success");
          req.flash("message", "Galeri berhasil ditambahkan");
          res.redirect("/admin/gallery");
        });
      });
    return false;
  }
  req.flash("color", "danger");
  req.flash("message", "Terjadi Kesalahan, Silahkan ulangi unggah data");
  res.redirect("/admin/gallery");
};

// exports.updateGallery = async (_, res) => {
//   try {
//     res.render("admin/pages/news/addNews");
//   } catch (error) {
//     res.json({ message: error.message });
//   }
// };

exports.deleteGallery = async (req, res) => {
  const id = req.params.id;

  gallery
    .destroy({
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        res.send({
          status: "success",
          message: "Data berhasil dihapus!",
        });
      } else {
        res.status(404).send({
          status: "failed",
          message: "Gagal Hapus. Data tidak ditemukan!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        status: "failed",
        // message: "Terjadi Kesalahan. Data gagal dihapus!",
        message: err.message,
      });
    });
};
