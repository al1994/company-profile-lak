const { news } = require("../../models");
const { DateFormat } = require("../../config/helperFunction.js");

exports.getNews = async (req, res) => {
  try {
    const getNews = await news.findAll({
      where: {
        isShow: "1",
        deleted: "0",
      },
      order: [["createdAT", "DESC"]],
    });

    if (getNews.length > 0) {
      res.render("admin/pages/news", {
        news: getNews,
        date: DateFormat,
        color: req.flash("color"),
        message: req.flash("message"),
      });
    } else {
      throw new Error("Tidak Terdapat Publikasi");
    }
  } catch (error) {
    res.render("admin/pages/news", {
      color: "danger",
      message: error.message,
      news: [],
    });
  }
};

exports.createNews = async (req, res) => {
  res.render("admin/pages/news/addNews", {
    colorFlash: req.flash("color"),
    statusFlash: req.flash("status"),
    messageFlash: req.flash("message"),
  });
};

exports.storeNews = async (req, res) => {
  if (req.body.images.length <= 0) {
    req.flash("color", "danger");
    req.flash("message", "Tidak terdapat unggahan file");
    res.redirect("/admin/publication/create");
  } else {
    const dataStore = {
      image: req.body.images[0],
      title: req.body.title,
      description: req.body.teks,
      reference: req.body.reference ? req.body.reference : "/",
    };

    news
      .create(dataStore)
      .then((data) => {
        req.flash("color", "success");
        req.flash("status", "Good Job..");
        req.flash("message", "Publikasi berhasil ditambahkan");
        res.redirect("/admin/publication");
      })
      .catch((err) => {
        req.flash("color", "danger");
        req.flash("message", "Terjadi Kesalahan, Silahkan ulangi unggah data");
        res.redirect("/admin/publication/create");
      });

    return false;
  }
};

exports.deleteNews = (req, res) => {
  const id = req.params.id;

  news
    .update(
      { deleted: "1" },
      {
        where: { id: id },
      }
    )
    .then((num) => {
      if (num == 1) {
        res.send({
          status: "success",
          message: "Data berhasil dihapus!",
        });
      } else {
        res.status(404).send({
          status: "failed",
          message: "Gagal Hapus. Data tidak ditemukan!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        status: "failed",
        message: "Terjadi Kesalahan. Data gagal dihapus!",
      });
    });
};
