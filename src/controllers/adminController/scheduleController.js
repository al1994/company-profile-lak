const { bimtek, schedule, location, pack } = require("../../models");
const { DateFormat, convertDate } = require("../../config/helperFunction.js");
const { dayNames } = require("../../config/const.js");

exports.getSchedule = async (req, res) => {
  try {
    const result = await Promise.all([
      schedule.findAll({
        include: [
          {
            model: bimtek,
          },
        ],
        where: {
          isShow: "1",
          deleted: "0",
          status: "open",
        },
        order: [["dateStart", "ASC"]],
      }),
      bimtek.findAll({
        where: {
          isShow: "1",
          deleted: "0",
        },
      }),
      location.findAll({
        where: {
          isShow: "1",
          deleted: "0",
        },
      }),
      pack.findAll(),
    ]);

    if (result[0].length > 0) {
      res.render("admin/pages/schedule/", {
        schedule: result[0],
        bimtek: result[1],
        location: result[2],
        pack: result[3],
        DateFormat: DateFormat,
        dayNames: dayNames,
        isSucces: true,
        color: req.flash("color"),
        message: req.flash("message"),
      });
    } else {
      throw {
        data: result[1],
        location: result[2],
        pack: result[3],
        dayNames: dayNames,
        message: "Tidak Terdapat Jadwal Bimtek",
      };
    }
  } catch (error) {
    res.render("admin/pages/schedule/", {
      isSucces: false,
      color: "danger",
      bimtek: error.data,
      message: error.message,
      location: error.location,
      pack: error.pack,
      dayNames: error.dayNames,
      schedule: [],
    });
  }
};

exports.storeSchedule = async (req, res) => {
  let data = {
    bimtekId: req.body.statusBimtek ? req.body.bimtekId : 0,
    servicePackId: req.body.pack,
    eventCity: req.body.eventCity,
    eventLocation: req.body.eventLocation,
    dateStart: req.body.dateStart,
    dateFinish: req.body.dateFinish,
  };

  if (req.body.pack == "1" || req.body.pack == "3") {
    data = {
      ...data,
      checkIn: req.body.checkIn,
      checkOut: req.body.checkOut,
    };
  }

  schedule
    .create(data)
    .then((data) => {
      req.flash("color", "success");
      req.flash("message", "Data berhasil ditambahkan");
      res.redirect("/admin/schedule/");
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", err.message);
      res.redirect("/admin/schedule/");
    });
};

exports.editSchedule = async (req, res) => {
  const result = await Promise.all([
    schedule.findOne({
      include: [
        {
          model: bimtek,
        },
      ],
      where: {
        id: req.params.id,
      },
    }),
    bimtek.findAll({
      where: {
        isShow: "1",
        deleted: "0",
      },
    }),
    location.findAll(),
    pack.findAll(),
  ]);

  res.render("admin/pages/schedule/scheduleEdit", {
    schedule: result[0],
    bimtek: result[1],
    location: result[2],
    pack: result[3],
    dayNames: dayNames,
    convertDate: convertDate,
  });
};

exports.updateSchedule = async (req, res) => {
  const data = {
    ...req.body,
    bimtekId: req.body.bimtekId ? req.body.bimtekId : 0,
    checkIn: req.body.pack != 2 ? req.body.checkIn : null,
    checkOut: req.body.pack != 2 ? req.body.checkOut : null,
    servicePackId: req.body.pack,
  };

  schedule
    .update(data, {
      where: { id: req.params.id },
    })
    .then((num) => {
      if (num == 1) {
        req.flash("color", "success");
        req.flash("message", "Data berhasil diperbarui");
        res.redirect("/admin/schedule");
      } else {
        res.status(404).send({
          status: "failed",
          message: "Gagal Diperbarui. Data tidak ditemukan!",
        });
      }
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", "Terjadi kesalahan. Data gagal diperbarui!");
      res.redirect("/admin/schedule/");
    });
};

exports.deleteSchedule = (req, res) => {
  const id = req.params.id;

  schedule
    .update(
      { deleted: "1" },
      {
        where: { id: id },
      }
    )
    .then((num) => {
      if (num == 1) {
        res.send({
          status: "success",
          message: "Data berhasil dihapus!",
        });
      } else {
        res.status(404).send({
          status: "failed",
          message: "Gagal Hapus. Data tidak ditemukan!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        status: "failed",
        message: "Terjadi Kesalahan. Data gagal dihapus!",
      });
    });
};
