const { bimtek, bimtek_sub } = require("../../models");
const { Op } = require("sequelize");

exports.getBimtek = async (req, res) => {
  try {
    const getBimtekSub = await bimtek_sub.findAll({
      where: {
        isShow: "1",
        deleted: "0",
      },
      include: {
        model: bimtek,
        where: {
          deleted: "0",
        },
        required: false,
      },
    });

    if (getBimtekSub.length > 0) {
      res.render("admin/pages/bimtek/", {
        subBimtek: getBimtekSub,
        color: req.flash("color"),
        message: req.flash("message"),
      });
    } else {
      throw new Error("Tidak Terdapat Daftar Bimtek");
    }
  } catch (error) {
    res.render("admin/pages/bimtek/", {
      color: "danger",
      message: error.message,
      subBimtek: [],
    });
  }
};

exports.storeBimtek = async (req, res) => {
  bimtek
    .create(req.body)
    .then((data) => {
      req.flash("color", "success");
      req.flash("message", "Data berhasil ditambahkan");
      res.redirect("/admin/bimtek/");
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", "Data gagal disimpan");
      res.redirect("/admin/bimtek/");
    });
};

exports.editBimtek = async (req, res) => {
  bimtek_sub
    .findAll({
      include: {
        model: bimtek,
      },
    })
    .then((data) => {
      if (data) {
        const edit = data.find((x) => x.id == req.params.id);

        console.log(data);

        res.render("admin/pages/bimtek/edit", {
          subBimtek: data,
          edit: edit,
          color: req.flash("color"),
          message: req.flash("message"),
        });
      } else {
        req.flash("color", "danger");
        req.flash("message", "Data sub bimtek tidak ditemukan");
        res.redirect("/admin/bimtek/");
      }
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", "Terjadi Kesalahan. Data gagal diambil");
      res.redirect("/admin/bimtek/");
    });
};

exports.updateBimtek = async (req, res) => {
  const id = req.params.id;

  bimtek
    .update(req.body, {
      where: { id: id },
    })
    .then((num) => {
      if (num == 1) {
        req.flash("color", "success");
        req.flash("message", "Data berhasil diperbarui");
        res.redirect("/admin/bimtek/");
      } else {
        res.status(404).send({
          status: "failed",
          message: "Gagal Diperbarui. Data tidak ditemukan!",
        });
      }
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", "Terjadi kesalahan. Data gagal diperbarui!");
      res.redirect("/admin/bimtek/");
    });
};

exports.deleteBimtek = (req, res) => {
  const id = req.params.id;

  bimtek
    .update(
      { deleted: "1" },
      {
        where: { id: id },
      }
    )
    .then((num) => {
      if (num == 1) {
        res.send({
          status: "success",
          message: "Data berhasil dihapus!",
        });
      } else {
        res.status(404).send({
          status: "failed",
          message: "Gagal Hapus. Data tidak ditemukan!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        status: "failed",
        message: "Terjadi Kesalahan. Data gagal dihapus!",
      });
    });
};
