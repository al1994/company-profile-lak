const { bimtek_sub, location, pack } = require("../../models");

exports.getDataDefault = async (req, res) => {
  try {
    const result = await Promise.all([
      bimtek_sub.findAll({
        where: {
          isShow: "1",
          deleted: "0",
        },
      }),
      location.findAll({
        where: {
          isShow: "1",
          deleted: "0",
        },
      }),
      pack.findAll(),
    ]);

    res.render("admin/pages/defaultData", {
      bimtek_sub: result[0],
      location: result[1],
      pack: result[2],
      color: req.flash("color"),
      statusFlash: req.flash("status"),
      message: req.flash("message"),
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

exports.storeSubBimtek = async (req, res) => {
  bimtek_sub
    .create(req.body)
    .then((data) => {
      req.flash("color", "success");
      req.flash("message", "Sub-Bimtek berhasil ditambahkan");
      res.redirect("/admin/default");
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", "Terjadi Kesalahan, Silahkan ulangi unggah data");
      res.redirect("/admin/default");
    });
};

exports.updateSubBimtek = async (req, res) => {
  bimtek_sub
    .update(req.body, {
      where: { id: req.body.id },
    })
    .then((num) => {
      if (num == 1) {
        req.flash("color", "success");
        req.flash("message", "Data Sub Bimtek Berhasil Diperbarui");
        res.redirect("/admin/default");
      } else {
        req.flash("color", "success");
        req.flash("message", "Data Sub Bimtek tidak ditemukan");
        res.redirect("/admin/default");
      }
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash(
        "message",
        "Terjadi kesalahan. silahkan kembali beberapa saat lagi"
      );
      res.redirect("/admin/default");
    });
};

exports.deleteSubBimtek = (req, res) => {
  bimtek_sub
    .update(
      { deleted: "1" },
      {
        where: { id: req.params.id },
      }
    )
    .then((num) => {
      if (num == 1) {
        req.flash("color", "success");
        req.flash("message", "Sub-Bimtek berhasil dihapus");
        res.redirect("/admin/default");
      } else {
        req.flash("color", "danger");
        req.flash("message", "Sub-Bimtek ini tidak ditemukan");
        res.redirect("/admin/default");
      }
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", "Terjadi kesalahan, server sedang sibuk");
      res.redirect("/admin/default");
    });
};

exports.storeLocation = async (req, res) => {
  location
    .create(req.body)
    .then((data) => {
      req.flash("color", "success");
      req.flash("message", "Kota pelaksanaan berhasil ditambahkan");
      res.redirect("/admin/default");
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", "Terjadi Kesalahan, Silahkan ulangi unggah data");
      res.redirect("/admin/default");
    });
};

exports.updateLocation = async (req, res) => {
  location
    .update(req.body, {
      where: { id: req.body.id },
    })
    .then((num) => {
      if (num == 1) {
        req.flash("color", "success");
        req.flash("message", "Data Kota Berhasil Diperbarui");
        res.redirect("/admin/default");
      } else {
        req.flash("color", "success");
        req.flash("message", "Data Kota tidak ditemukan");
        res.redirect("/admin/default");
      }
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash(
        "message",
        "Terjadi kesalahan. silahkan kembali beberapa saat lagi"
      );
      res.redirect("/admin/default");
    });
};

exports.deleteLocation = (req, res) => {
  location
    .update(
      { deleted: "1" },
      {
        where: { id: req.params.id },
      }
    )
    .then((num) => {
      if (num == 1) {
        req.flash("color", "success");
        req.flash("message", "Kota pelaksanaan berhasil dihapus");
        res.redirect("/admin/default");
      } else {
        req.flash("color", "danger");
        req.flash("message", "Kota pelaksanaan ini tidak ditemukan");
        res.redirect("/admin/default");
      }
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", "Terjadi kesalahan, server sedang sibuk");
      res.redirect("/admin/default");
    });
};

exports.updatePack = async (req, res) => {
  pack
    .update(req.body, {
      where: { id: req.body.id },
    })
    .then((num) => {
      if (num == 1) {
        req.flash("color", "success");
        req.flash("message", "Data Paket Layanan Berhasil Diperbarui");
        res.redirect("/admin/default");
      } else {
        req.flash("color", "success");
        req.flash("message", "Data Paket Layanan tidak ditemukan");
        res.redirect("/admin/default");
      }
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash(
        "message",
        "Terjadi kesalahan. silahkan kembali beberapa saat lagi"
      );
      res.redirect("/admin/default");
    });
};
