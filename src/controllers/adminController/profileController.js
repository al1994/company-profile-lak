const { profile } = require("../../models");
const {
  DateFormat,
  setCodeCountry,
} = require("../../config/helperFunction.js");

exports.getProfile = async (req, res) => {
  try {
    const getProfile = await profile.findOne();

    res.render("admin/pages/profile", {
      profile: getProfile,
      colorFlash: req.flash("color"),
      statusFlash: req.flash("status"),
      messageFlash: req.flash("message"),
      date: DateFormat,
    });
  } catch (error) {
    res.json({ message: error.message });
  }
};

// exports.createProfile = async (req, res) => {
//   res.render("admin/pages/news/addNews", {
//     colorFlash: req.flash("color"),
//     statusFlash: req.flash("status"),
//     messageFlash: req.flash("message"),
//   });
// };

exports.storeProfile = async (req, res) => {
  // if (req.body.images.length <= 0) {
  //   req.flash("color", "danger");
  //   req.flash("message", "Tidak terdapat unggahan file");
  //   res.redirect("/admin/profile");
  //   return false;
  // } else {
  const dataStore = {
    // imageHeader: req.body.images[0],
    title: req.body.title,
    address: req.body.address,
    city: req.body.city,
    district: req.body.district,
    postcode: req.body.postcode,
    description: req.body.teks,
    email: req.body.email,
    phone: setCodeCountry(req.body.phone),
  };

  profile
    .update(dataStore, {
      where: { id: 1 },
    })
    .then((data) => {
      req.flash("color", "success");
      req.flash("status", "Good Job..");
      req.flash("message", "Profile berhasil diubah");
      res.redirect("/admin/profile");
    })
    .catch((err) => {
      req.flash("color", "danger");
      req.flash("message", "Terjadi Kesalahan, Silahkan ulangi ubah data");
      res.redirect("/admin/profile");
    });

  return false;
  // }
};

exports.deleteProfile = (req, res) => {
  const id = req.params.id;

  News.destroy({
    where: { id: id },
  })
    .then((num) => {
      if (num == 1) {
        res.send({
          status: "success",
          message: "Data berhasil dihapus!",
        });
      } else {
        res.status(404).send({
          status: "failed",
          message: "Gagal Hapus. Data tidak ditemukan!",
        });
      }
    })
    .catch((err) => {
      res.status(500).send({
        status: "failed",
        message: "Terjadi Kesalahan. Data gagal dihapus!",
      });
    });
};
