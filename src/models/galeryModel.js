module.exports = (sequelize, Sequelize) => {
  const Galery = sequelize.define(
    "galery",
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
      },
      title: {
        type: Sequelize.DataTypes.STRING,
      },
      isShow: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
      deleted: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
      tag: {
        type: Sequelize.DataTypes.STRING,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return Galery;
};
