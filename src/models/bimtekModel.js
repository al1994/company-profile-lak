module.exports = (sequelize, Sequelize) => {
  const Bimtek = sequelize.define(
    "bimtek",
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
      },
      bimtekSubId: {
        type: Sequelize.TINYINT,
      },
      title: {
        type: Sequelize.DataTypes.STRING,
      },
      description: {
        type: Sequelize.DataTypes.TEXT,
      },
      isShow: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
      deleted: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return Bimtek;
};
