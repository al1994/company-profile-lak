const bcrypt = require("bcrypt");

module.exports = (sequelize, Sequelize) => {
  const users = sequelize.define(
    "users",
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
      },
      username: {
        type: Sequelize.DataTypes.STRING,
      },
      password: {
        type: Sequelize.DataTypes.TEXT,
      },
    },
    {
      hooks: {
        beforeCreate: async (user) => {
          if (user.password) {
            const salt = bcrypt.genSaltSync(10, "a");
            user.password = bcrypt.hashSync(user.password, salt);
            console.log(user.password);
          }
        },
        beforeUpdate: async (user) => {
          if (user.password) {
            const salt = bcrypt.genSaltSync(10, "a");
            user.password = bcrypt.hashSync(user.password, salt);
            console.log(user.password);
          }
        },
      },
    }
  );
  users.validPassword = (enteredPassword, originalPassword) => {
    return new Promise((resolve) => {
      bcrypt.compare(enteredPassword, originalPassword, (err, res) => {
        resolve(res);
      });
    });
  };

  return users;
};

// const validPassword = (enteredPassword, originalPassword) => {
//   return new Promise((resolve) => {
//     bcrypt.compare(enteredPassword, originalPassword, (err, res) => {
//       resolve(res);
//     });
//   });
// };

// module.exports = {
//   User,
//   validPassword,
// };
