module.exports = (sequelize, Sequelize) => {
  const SubBimtek = sequelize.define(
    "bimtek_sub",
    {
      id: {
        type: Sequelize.TINYINT.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      sub_bimtek: {
        type: Sequelize.DataTypes.STRING,
      },
      isShow: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
      deleted: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return SubBimtek;
};
