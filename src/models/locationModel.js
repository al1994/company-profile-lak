module.exports = (sequelize, Sequelize) => {
  const Location = sequelize.define(
    "location",
    {
      id: {
        type: Sequelize.TINYINT.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      location: {
        type: Sequelize.DataTypes.STRING,
      },
      isShow: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
      deleted: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return Location;
};
