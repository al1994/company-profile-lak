module.exports = (sequelize, Sequelize) => {
  const profile = sequelize.define(
    "profile",
    {
      imageHeader: {
        type: Sequelize.DataTypes.TEXT,
      },
      title: {
        type: Sequelize.DataTypes.STRING,
      },
      address: {
        type: Sequelize.DataTypes.TEXT,
      },
      city: {
        type: Sequelize.DataTypes.STRING,
      },
      district: {
        type: Sequelize.DataTypes.STRING,
      },
      postcode: {
        type: Sequelize.DataTypes.STRING,
      },
      description: {
        type: Sequelize.DataTypes.TEXT,
      },
      email: {
        type: Sequelize.DataTypes.STRING,
      },
      phone: {
        type: Sequelize.DataTypes.STRING,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return profile;
};
