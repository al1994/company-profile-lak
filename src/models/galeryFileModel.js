module.exports = (sequelize, Sequelize) => {
  const GaleryFile = sequelize.define(
    "gallery_file",
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        primaryKey: true,
      },
      galeryId: {
        type: Sequelize.UUID,
      },
      image: {
        type: Sequelize.DataTypes.TEXT,
      },
      isShow: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
      deleted: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return GaleryFile;
};
