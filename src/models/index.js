const dbConfig = require("../config/database.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: 0,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle,
  },
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./userModel.js")(sequelize, Sequelize);
db.bimtek_sub = require("./bimtekSubModel")(sequelize, Sequelize);
db.bimtek = require("./bimtekModel.js")(sequelize, Sequelize);
db.schedule = require("./scheduleBimtekModel")(sequelize, Sequelize);
db.gallery = require("./galeryModel")(sequelize, Sequelize);
db.galleryFile = require("./galeryFileModel.js")(sequelize, Sequelize);
db.news = require("./newsModel")(sequelize, Sequelize);
db.location = require("./locationModel")(sequelize, Sequelize);
db.profile = require("./profileModel")(sequelize, Sequelize);
db.pack = require("./packModel")(sequelize, Sequelize);

db.bimtek_sub.hasMany(db.bimtek, {
  foreignKey: "bimtekSubId",
});
db.bimtek.belongsTo(db.bimtek_sub, {
  onDelete: "CASCADE",
});

db.bimtek.hasMany(db.schedule, {
  foreignKey: "bimtekId",
});
db.schedule.belongsTo(db.bimtek, {
  onDelete: "CASCADE",
});

db.pack.hasOne(db.schedule);
db.schedule.belongsTo(db.pack, {
  foreignKey: "servicePackId",
});

// db.location.hasMany(db.bimtek, {
//   foreignKey: "bimtekSubId",
// });
// db.bimtek.belongsTo(db.location, {
//   onDelete: "CASCADE",
// });

db.gallery.hasMany(db.galleryFile, {
  foreignKey: "galeryId",
});
db.galleryFile.belongsTo(db.gallery, {
  onDelete: "CASCADE",
});

module.exports = db;
