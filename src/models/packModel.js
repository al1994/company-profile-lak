module.exports = (sequelize, Sequelize) => {
  const Pack = sequelize.define(
    "service_pack",
    {
      id: {
        type: Sequelize.TINYINT.UNSIGNED,
        autoIncrement: true,
        primaryKey: true,
      },
      pack: {
        type: Sequelize.DataTypes.STRING,
      },
      cost: {
        type: Sequelize.DataTypes.STRING,
      },
      description: {
        type: Sequelize.DataTypes.TEXT,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return Pack;
};
