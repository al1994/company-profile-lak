module.exports = (sequelize, Sequelize) => {
  const BimtekSchedule = sequelize.define(
    "bimtek_schedule",
    {
      id: {
        type: Sequelize.UUID,
        defaultValue: Sequelize.UUIDV4,
        allowNull: false,
        primaryKey: true,
      },
      bimtekId: {
        type: Sequelize.DataTypes.UUID,
      },
      servicePackId: {
        type: Sequelize.DataTypes.ENUM("1", "2", "3"),
      },
      dateStart: {
        type: Sequelize.DataTypes.DATE,
      },
      dateFinish: {
        type: Sequelize.DataTypes.DATE,
      },
      checkIn: {
        type: Sequelize.DataTypes.STRING,
      },
      checkOut: {
        type: Sequelize.DataTypes.STRING,
      },
      eventCity: {
        type: Sequelize.DataTypes.STRING,
      },
      eventLocation: {
        type: Sequelize.DataTypes.TEXT,
      },
      status: {
        type: Sequelize.DataTypes.ENUM("open", "close", "finish"),
      },
      isShow: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
      deleted: {
        type: Sequelize.DataTypes.BOOLEAN,
      },
    },
    {
      freezeTableName: true,
    }
  );

  return BimtekSchedule;
};
