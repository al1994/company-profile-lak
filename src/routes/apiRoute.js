const express = require("express");
const router = express.Router();

const { isLogin } = require("../config/verify.js");

const {
  getScheduleAjax,
  closeSchedule,
} = require("../controllers/ajaxController.js");

router.get("/bimtek", getScheduleAjax);
router.post("/scheduleCLose/:id", isLogin, closeSchedule);

module.exports = router;
