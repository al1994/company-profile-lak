const express = require("express");
const router = express.Router();

const {
  getGallery,
  storeGallery,
  // updateGallery,
  deleteGallery,
} = require("../../controllers/adminController/galleryController.js");

const {
  uploadImages,
  resizeImages,
} = require("../../controllers/adminController/uploadImage.js");

router.get("/", getGallery);
router.post("/", uploadImages, resizeImages, storeGallery);
// router.post("/:id", updateGallery);
router.delete("/:id", deleteGallery);

module.exports = router;
