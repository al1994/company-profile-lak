const express = require("express");
const router = express.Router();

const {
  getProfile,
  // createProfile,
  storeProfile,
  deleteProfile,
} = require("../../controllers/adminController/profileController.js");

// const {
//   uploadImages,
//   resizeImages,
// } = require("../../controllers/adminController/uploadImage.js");

router.get("/", getProfile);
router.post("/", storeProfile);
// router.get("/create", createProfile);
router.delete("/:id", deleteProfile);
// router.put("/:id", finishSchedule);

module.exports = router;
