const express = require("express");
const router = express.Router();

const {
  getBimtek,
  storeBimtek,
  editBimtek,
  updateBimtek,
  deleteBimtek,
} = require("../../controllers/adminController/bimtekController.js");

router.get("/", getBimtek);
router.post("/", storeBimtek);
router.get("/:id/edit", editBimtek);
router.post("/:id", updateBimtek);
router.delete("/:id", deleteBimtek);
// Router.put("/:id", finishSchedule);

module.exports = router;
