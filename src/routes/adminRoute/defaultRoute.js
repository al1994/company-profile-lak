const express = require("express");
const router = express.Router();

const {
  getDataDefault,
  storeSubBimtek,
  updateSubBimtek,
  deleteSubBimtek,
  storeLocation,
  updateLocation,
  deleteLocation,
  updatePack,
} = require("../../controllers/adminController/defaultDataController.js");

// const {
//   uploadImages,
//   resizeImages,
// } = require("../../controllers/adminController/uploadImage.js");

router.get("/", getDataDefault);
router.post("/subbimtek", storeSubBimtek);
router.post("/subbimtek/:id", updateSubBimtek);
router.get("/subbimtek/:id", deleteSubBimtek);
router.post("/location", storeLocation);
router.post("/location/:id", updateLocation);
router.get("/location/:id", deleteLocation);
router.post("/pack", updatePack);

module.exports = router;
