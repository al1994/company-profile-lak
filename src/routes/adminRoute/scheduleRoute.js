const express = require("express");
const router = express.Router();

const {
  getSchedule,
  // createSchedule,
  editSchedule,
  updateSchedule,
  storeSchedule,
  deleteSchedule,
} = require("../../controllers/adminController/scheduleController.js");

router.get("/", getSchedule);
router.post("/", storeSchedule);
router.get("/:id/edit", editSchedule);
router.post("/:id", updateSchedule);
router.delete("/:id", deleteSchedule);

module.exports = router;
