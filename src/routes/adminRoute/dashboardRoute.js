const express = require("express");
const router = express.Router();

const {
  getDashboard,
  //   storeBimtek,
  //   updateBimtek,
  //   deleteBimtek,
} = require("../../controllers/adminController/dashboardController.js");

router.get("/", getDashboard);
// router.post("/", storeBimtek);
// router.post("/:id", updateBimtek);
// router.delete("/:id", deleteBimtek);
// Router.put("/:id", finishSchedule);

module.exports = router;
