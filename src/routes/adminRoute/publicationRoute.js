const express = require("express");
const router = express.Router();

const {
  getNews,
  createNews,
  storeNews,
  deleteNews,
} = require("../../controllers/adminController/newsController.js");

const {
  uploadImages,
  resizeImages,
} = require("../../controllers/adminController/uploadImage.js");

router.get("/", getNews);
router.post("/", uploadImages, resizeImages, storeNews);
router.get("/create", createNews);
router.delete("/:id", deleteNews);

module.exports = router;
