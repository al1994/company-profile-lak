const express = require("express");
const router = express.Router();

const Login = require("../controllers/loginController.js");
const { isLogout } = require("../config/verify.js");

router.get("/", isLogout, Login.login);
router.post("/", isLogout, Login.auth);
router.get("/logout", Login.logout);

module.exports = router;
