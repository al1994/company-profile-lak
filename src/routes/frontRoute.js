const express = require("express");
const router = express.Router();

const {
  getProfile,
  getAbout,
  getPublication,
  showPublication,
  getBimtek,
  showBimtek,
  getSchedule,
  showSchedule,
  getGalery,
} = require("../controllers/frontController.js");

router.get("/", getProfile);
router.get("/about", getAbout);
router.get("/publication", getPublication);
router.get("/publication/:id", showPublication);
router.get("/bimtek", getBimtek);
router.get("/bimtek/:id", showBimtek);
router.get("/schedule", getSchedule);
router.get("/schedule/:id", showSchedule);
router.get("/galery", getGalery);

module.exports = router;
