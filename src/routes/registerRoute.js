const express = require("express");
const router = express.Router();

const { index, store } = require("../controllers/registerController.js");

router.get("/", index); //to register html
router.post("/", store); //store form from register

module.exports = router;
