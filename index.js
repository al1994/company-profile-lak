const express = require("express");
const session = require("express-session");
const reqFlash = require("req-flash");

const cors = require("cors");
// const path = require("path");

const login = require("./src/routes/loginRoute.js");
const register = require("./src/routes/registerRoute.js");
const frontRoute = require("./src/routes/frontRoute.js");
const apiRoute = require("./src/routes/apiRoute.js");

const publicationRoute = require("./src/routes/adminRoute/publicationRoute.js");
const scheduleRoute = require("./src/routes/adminRoute/scheduleRoute.js");
const bimtekRoute = require("./src/routes/adminRoute/bimtekRoute.js");
const galleryRoute = require("./src/routes/adminRoute/galleryRoute.js");
const profileRoute = require("./src/routes/adminRoute/profileRoute.js");
// const dashboardRoute = require("./src/routes/adminRoute/dashboardRoute");
const defaultRoute = require("./src/routes/adminRoute/defaultRoute");

const { isLogin } = require("./src/config/verify.js");

const app = express();
app.use(
  session({
    resave: false,
    saveUninitialized: false,
    secret: "t@1k0ch3ng",
    name: "secretName",
    cookie: {
      sameSite: true,
      maxAge: 1800000,
    },
  })
);

app.set("view engine", "ejs");
app.use(reqFlash());
app.use("/assets", express.static("public"));
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.use("/login", login);
app.use("/register", register);

app.use("/admin/publication", isLogin, publicationRoute);
app.use("/admin/schedule", isLogin, scheduleRoute);
app.use("/admin/bimtek", isLogin, bimtekRoute);
app.use("/admin/gallery", isLogin, galleryRoute);
app.use("/admin/profile", isLogin, profileRoute);
// app.use("/admin/dashboard", isLogin, dashboardRoute);
app.use("/admin/default", isLogin, defaultRoute);

app.use("/api", apiRoute);
app.use("/", frontRoute);

app.get("*", function (req, res) {
  res.render("pages/404");
});

app.listen(5000, () => console.log("Server running at port 5000"));

// 3@x6G{2dK-fO
